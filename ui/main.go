package main

import (
	"fmt"
	pb "ftsystem.com/wf-engine-stub/proto"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"log"
	"time"
)

const (
	coreAddress = "localhost:50052"
)

func main() {
	// Set up a connection to the server.
	conn, err := grpc.Dial(coreAddress, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()

	client := pb.NewWorkflowManagerClient(conn)

	StartWorkflow(client, "ReadResults")
	time.Sleep(time.Second * 10)
	StopWorkflow(client, "ReadResults")
	//time.Sleep(time.Second * 2)
	//StartWorkflow(client, "ReadResults")

}

func StartWorkflow(client pb.WorkflowManagerClient, name string) {
	r, err := client.Start(context.Background(), &pb.Request{Param: name})
	if err == nil {
		fmt.Printf("\nCall %s reply %s\n", name, r.Param)
	} else {
		fmt.Printf("\nERROR %s\n", err.Error())
	}
}

func StopWorkflow(client pb.WorkflowManagerClient, name string) {
	r, err := client.Stop(context.Background(), &pb.Request{Param: name})
	if err == nil {
		fmt.Printf("\nCall %s reply %s", name, r.Param)
	} else {
		fmt.Printf("\nERROR %s", err.Error())
	}
}
