package command_test

import (
	"ftsystem.com/wf-engine-stub/engine/command"
	"ftsystem.com/wf-engine-stub/engine/interfaces"
	. "gopkg.in/check.v1"
	"testing"
)

func TestResponse(t *testing.T) { TestingT(t) }

type ResponseSuite struct{}

var _ = Suite(&ResponseSuite{})

func (s *ResponseSuite) TestNewGoodResponse(c *C) {

	r := command.NewResponse(interfaces.OK, "data", "")
	c.Assert(r, FitsTypeOf, r)
	c.Assert(r.Data(), DeepEquals, "data")
	c.Assert(r.ErrorMessage(), DeepEquals, "")
	c.Assert(r.Code(), DeepEquals, interfaces.OK)
}

func (s *ResponseSuite) TestNewBadResponse(c *C) {

	r := command.NewResponse(1, nil, "Error message")
	c.Assert(r, FitsTypeOf, r)
	c.Assert(r.Data(), IsNil)
	c.Assert(r.ErrorMessage(), DeepEquals, "Error message")
	c.Assert(r.Code(), DeepEquals, interfaces.KO)
}
