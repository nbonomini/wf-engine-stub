package scheduler

import (
	"ftsystem.com/wf-engine-stub/engine/interfaces"
	"sync"
	"time"
)

const (
	NextExecutionWaitTime = 200 * time.Millisecond
)

type Scheduler struct {
	sync.RWMutex
	Queue        []interfaces.IWorkflow
	CurrentIndex int
	Logger       interfaces.ILogger
}

func NewScheduler(logger interfaces.ILogger) *Scheduler {
	return &Scheduler{
		Queue:        []interfaces.IWorkflow{},
		CurrentIndex: 0,
		Logger:       logger,
	}
}

func (s *Scheduler) AddInstance(instance interfaces.IWorkflow) {
	queueIsEmpty := len(s.Queue) == 0
	s.Lock()
	s.Queue = append(s.Queue, instance)
	s.Unlock()
	if queueIsEmpty {
		go s.ExecuteCurrentStep()
	}
}

func (s *Scheduler) ExecuteCurrentStep() {
	wi := s.GetCurrentInstance()

	if wi == nil {
		return
	}

	time.Sleep(NextExecutionWaitTime)
	wi.Run()

	workflowIsCompleted := wi.IsCompleted()

	if workflowIsCompleted {
		// TODO check if it can be removed now that "init" can start from "completed" state
		err := wi.Reset()
		if err != nil {
			s.Logger.Error(err.Error())
		}
		s.RemoveInstance(s.CurrentIndex)
	}
	s.setNextIndex(workflowIsCompleted)
	s.ExecuteCurrentStep()
}

func (s *Scheduler) RemoveInstance(instanceIndex int) {
	s.Lock()
	s.Queue = append(s.Queue[:instanceIndex], s.Queue[instanceIndex+1:]...)
	s.Unlock()
}

func (s *Scheduler) GetCurrentInstance() interfaces.IWorkflow {
	s.RLock()
	defer s.RUnlock()
	if len(s.Queue) == 0 {
		return nil
	}
	return s.Queue[s.CurrentIndex]
}

func (s *Scheduler) setNextIndex(workflowIsCompleted bool) {
	increment := 1
	if workflowIsCompleted {
		increment = 0
	}
	nextIndex := s.CurrentIndex + increment
	s.RLock()
	if nextIndex >= len(s.Queue) {
		nextIndex = 0
	}
	s.RUnlock()
	s.CurrentIndex = nextIndex
}
