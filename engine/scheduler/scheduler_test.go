package scheduler_test

import (
	"context"
	"ftsystem.com/wf-engine-stub/engine/engine"
	"ftsystem.com/wf-engine-stub/engine/interfaces"
	"ftsystem.com/wf-engine-stub/engine/log"
	"ftsystem.com/wf-engine-stub/engine/scheduler"
	"ftsystem.com/wf-engine-stub/engine/workflow"
	"github.com/looplab/fsm"
	. "gopkg.in/check.v1"
	"testing"
	"time"
)

func TestScheduler(t *testing.T) { TestingT(t) }

type SchedulerSuite struct{}

var _ = Suite(&SchedulerSuite{})

var myWorkflowExecutedEvents []string
var brokenWorkflowExecutedEvents []string

func (s *SchedulerSuite) SetUpTest(c *C) {
	myWorkflowExecutedEvents = []string{}
	brokenWorkflowExecutedEvents = []string{}
}

func createFakeEngine() *engine.Engine {
	channels := map[string]interfaces.IChannel{}
	getWorkflowInstanceFn := func(ctx context.Context, name string, engine *engine.Engine) interfaces.IWorkflow {
		if name == "brokenResetWorkflow" {
			return createBrokenResetWorkflowInstance(engine, name)
		}
		return createMyWorkflowInstance(engine, name)
	}
	return engine.NewEngine(channels, "randomDeviceKey", getWorkflowInstanceFn, &log.Logger{})
}

func createMyWorkflowInstance(engine *engine.Engine, name string) interfaces.IWorkflow {
	w := workflow.NewSyncWorkflow(engine, name)

	w.FSM = fsm.NewFSM(
		"idle",
		fsm.Events{
			{Name: "init", Src: []string{"idle"}, Dst: "initializing"},
			{Name: "start", Src: []string{"initializing"}, Dst: "started"},
			{Name: "stop", Src: []string{"started"}, Dst: "idle"},
			{Name: "complete", Src: []string{"started"}, Dst: "completed"},
			{Name: "reset", Src: []string{"completed"}, Dst: "idle"},
		},
		fsm.Callbacks{
			"after_event": func(e *fsm.Event) {
				myWorkflowExecutedEvents = append(myWorkflowExecutedEvents, e.Event)
			},
		},
	)

	w.Triggers = interfaces.Triggers{
		"initializing": "start",
		"started":      "complete",
	}

	return w
}

func createBrokenResetWorkflowInstance(engine *engine.Engine, name string) interfaces.IWorkflow {
	w := workflow.NewSyncWorkflow(engine, name)

	w.FSM = fsm.NewFSM(
		"idle",
		fsm.Events{
			{Name: "init", Src: []string{"idle"}, Dst: "initializing"},
			{Name: "start", Src: []string{"initializing"}, Dst: "started"},
			{Name: "stop", Src: []string{"started"}, Dst: "idle"},
			{Name: "complete", Src: []string{"started"}, Dst: "completed"},
			{Name: "reset", Src: []string{"idle"}, Dst: "idle"},
		},
		fsm.Callbacks{
			"after_event": func(e *fsm.Event) {
				brokenWorkflowExecutedEvents = append(brokenWorkflowExecutedEvents, e.Event)
			},
		},
	)

	w.Triggers = interfaces.Triggers{
		"initializing": "start",
		"started":      "complete",
	}

	return w
}

func (s *SchedulerSuite) TestNewScheduler(c *C) {
	schedulerInstance := scheduler.NewScheduler(&log.Logger{})
	var expectedRequest *scheduler.Scheduler
	c.Assert(schedulerInstance, FitsTypeOf, expectedRequest)
}

func (s *SchedulerSuite) TestExecuteCurrentStep(c *C) {
	ctx := context.Background()
	schedulerInstance := scheduler.NewScheduler(&log.Logger{})
	engineInstance := createFakeEngine()
	var wfInstance interfaces.IWorkflow
	wfInstance = engineInstance.GetWorkflowInstance(ctx, "myWorkflow")
	schedulerInstance.AddInstance(wfInstance)
	c.Assert(len(schedulerInstance.Queue), Equals, 1)
	// workflow should start running
	time.Sleep(scheduler.NextExecutionWaitTime*2 + 500*time.Millisecond)
	c.Assert(len(schedulerInstance.Queue), Equals, 0)
	c.Assert(myWorkflowExecutedEvents, DeepEquals, []string{"init", "start", "complete", "reset"})
}

func (s *SchedulerSuite) TestResetDuringExecuteCurrentStep(c *C) {
	ctx := context.Background()
	schedulerInstance := scheduler.NewScheduler(&log.Logger{})
	engineInstance := createFakeEngine()
	var wfInstance interfaces.IWorkflow
	wfInstance = engineInstance.GetWorkflowInstance(ctx, "brokenResetWorkflow")
	schedulerInstance.AddInstance(wfInstance)
	c.Assert(len(schedulerInstance.Queue), Equals, 1)
	// workflow should start running
	time.Sleep(scheduler.NextExecutionWaitTime*3 + 100*time.Millisecond)
	c.Assert(len(brokenWorkflowExecutedEvents) > 0, Equals, true)
}
