package engine

import (
	"context"
	"fmt"
	"ftsystem.com/wf-engine-stub/engine/interfaces"
	"ftsystem.com/wf-engine-stub/engine/scheduler"
	"ftsystem.com/wf-engine-stub/engine/workflow"
)

const (
	maxResponseChannelBuffers = 100
)

type Engine struct {
	WorkflowInstances     map[string]interfaces.IWorkflow
	ResponseChannel       chan interfaces.IWfResponse
	DeviceChannels        map[string]interfaces.IChannel
	DeviceKey             string
	Logger                interfaces.ILogger
	scheduler             *scheduler.Scheduler
	getWorkflowInstanceFn GetWorkflowInstanceFn
	ctx                   context.Context
	cancelCtx             context.CancelFunc
	IsRunning             bool
}

type GetWorkflowInstanceFn func(ctx context.Context, name string, engine *Engine) interfaces.IWorkflow

func NewEngine(deviceChannels map[string]interfaces.IChannel, deviceKey string, getWorkflowInstanceFn GetWorkflowInstanceFn, logger interfaces.ILogger) *Engine {

	_ctx, _cancel := context.WithCancel(context.Background())

	return &Engine{
		WorkflowInstances:     make(map[string]interfaces.IWorkflow),
		ResponseChannel:       make(chan interfaces.IWfResponse, maxResponseChannelBuffers),
		DeviceChannels:        deviceChannels,
		DeviceKey:             deviceKey,
		scheduler:             scheduler.NewScheduler(logger),
		getWorkflowInstanceFn: getWorkflowInstanceFn,
		Logger:                logger,
		ctx:                   _ctx,
		cancelCtx:             _cancel,
		IsRunning:             false,
	}
}

func (e *Engine) Start() {
	fmt.Println("Start engine")
	go e.waitWorkflowResponse()
}

func (e *Engine) Stop() {
	fmt.Println("Stop engine")
	e.cancelCtx()
}

func (e *Engine) GetResponseChannel() chan interfaces.IWfResponse {
	return e.ResponseChannel
}

func (e *Engine) GetDeviceChannels() map[string]interfaces.IChannel {
	return e.DeviceChannels
}

func (e *Engine) stopWorkflows() {
	for _, w := range e.WorkflowInstances {
		w.Stop()
	}
}

func (e *Engine) waitWorkflowResponse() {
	var msg string
	for {
		select {
		case <-e.ctx.Done():
			e.stopWorkflows()
			return

		case r := <-e.ResponseChannel:
			response := r.(workflow.Response)
			if response.Code == interfaces.OK {
				msg = response.Result.(string)
			} else {
				msg = "ERROR: " + response.ErrorMessage
				e.Logger.Error(msg)
			}
			// TODO send message broadcasting to registered microservices
			//e.Logger.Info(msg)
		default:
		}
	}
}

func (e *Engine) workflowInstances(name string) (interfaces.IWorkflow, bool) {
	wf, ok := e.WorkflowInstances[name]
	return wf, ok
}

func (e *Engine) GetWorkflowInstance(ctx context.Context, name string) interfaces.IWorkflow {
	workflowInstance, exists := e.workflowInstances(name)
	if exists {
		return workflowInstance
	}

	wfInstance := e.getWorkflowInstanceFn(ctx, name, e)
	e.WorkflowInstances[name] = wfInstance
	return wfInstance
}

func (e *Engine) StartWorkflow(ctx context.Context, name string) string {
	wf := e.GetWorkflowInstance(ctx, name)
	s := e.runWorkflow(wf)
	return s
}

func (e *Engine) runWorkflow(wf interfaces.IWorkflow) string {
	if wf.IsRunning() {
		e.Logger.Info("Workflow already running")
		return "Workflow already running"
	}

	if wf.IsSync() {
		e.scheduler.AddInstance(wf)
	} else {
		go wf.Run()
	}

	e.Logger.Info("Workflow started")

	return "Workflow started"
}
