package engine_test

import (
	"context"
	"fmt"
	"ftsystem.com/wf-engine-stub/engine/channel"
	"ftsystem.com/wf-engine-stub/engine/engine"
	"ftsystem.com/wf-engine-stub/engine/interfaces"
	"ftsystem.com/wf-engine-stub/engine/log"
	"ftsystem.com/wf-engine-stub/engine/workflow"
	"github.com/looplab/fsm"
	"google.golang.org/grpc"
	. "gopkg.in/check.v1"
	"testing"
	"time"
)

func TestEngine(t *testing.T) { TestingT(t) }

type EngineSuite struct{}

var _ = Suite(&EngineSuite{})

func getChannels() map[string]interfaces.IChannel {

	channels := map[string]interfaces.IChannel{}
	fn := func(connInterface grpc.ClientConnInterface) interface{} {
		return ""
	}
	ch, _ := channel.NewGrpcChannel("127.0.0.1:51000", fn)
	channels["grpc"] = ch
	return channels
}

func createGoodAsyncWorkflowInstance(engine *engine.Engine, name string) interfaces.IWorkflow {
	builder := workflow.NewWorkflowBuilder(workflow.Cyclic)
	wf, _ := builder.
		StartWith(&workflow.StartingEvent{
			Name: "start",
			To:   "started",
		}).
		RegisterEvent(&workflow.Event{
			From: "started",
			Name: "execute",
			To:   "executed",
		}).
		EndWhen("executed").
		AfterEventCallback("execute", func(e *fsm.Event) {
			engine.GetResponseChannel() <- workflow.Response{
				Code:         interfaces.OK,
				ErrorMessage: "",
				Result:       "Test Result OK",
			}
		}).
		Build(engine, workflow.Async, name)
	return wf
}

func createGoodSyncWorkflowInstance(engine *engine.Engine, name string) interfaces.IWorkflow {
	builder := workflow.NewWorkflowBuilder(workflow.Cyclic)
	wf, errors := builder.
		StartWith(&workflow.StartingEvent{
			Name: "start",
			To:   "started",
		}).
		RegisterEvent(&workflow.Event{
			From: "started",
			Name: "execute",
			To:   "executed",
		}).
		EndWhen("executed").
		AfterEventCallback("execute", func(e *fsm.Event) {
			engine.GetResponseChannel() <- workflow.Response{
				Code:         interfaces.OK,
				ErrorMessage: "",
				Result:       "Test Result OK",
			}
		}).Build(engine, workflow.Sync, name)
	if errors != nil {
		panic(errors)
	}
	return wf
}

func createBadSyncWorkflowInstance(engine *engine.Engine, name string) interfaces.IWorkflow {
	builder := workflow.NewWorkflowBuilder(workflow.Cyclic)
	wf, errors := builder.
		StartWith(&workflow.StartingEvent{
			Name: "start",
			To:   "started",
		}).
		RegisterEvent(&workflow.Event{
			From: "started",
			Name: "execute",
			To:   "executed",
		}).
		EndWhen("executed").
		AfterEventCallback("execute", func(e *fsm.Event) {
			engine.GetResponseChannel() <- workflow.Response{
				Code:         interfaces.KO,
				ErrorMessage: "Test Result KO",
				Result:       "",
			}
		}).Build(engine, workflow.Sync, name)
	if errors != nil {
		panic(errors)
	}
	return wf
}

func (s *EngineSuite) TestNewEngine(c *C) {
	getWorkflowInstanceFn := func(ctx context.Context, name string, engine *engine.Engine) interfaces.IWorkflow {
		return createGoodAsyncWorkflowInstance(engine, name)
	}
	e := engine.NewEngine(getChannels(), "if", getWorkflowInstanceFn, &log.Logger{})
	c.Assert(e, FitsTypeOf, e)
}

func (s *EngineSuite) TestStartAsyncWorkflow(c *C) {
	getWorkflowInstanceFn := func(ctx context.Context, name string, engine *engine.Engine) interfaces.IWorkflow {
		return createGoodAsyncWorkflowInstance(engine, name)
	}
	e := engine.NewEngine(getChannels(), "if", getWorkflowInstanceFn, &log.Logger{})

	ctx, _ := context.WithCancel(context.Background())

	e.Start()
	e.StartWorkflow(ctx, "WfTest")
	wf := e.GetWorkflowInstance(ctx, "WfTest")
	var wfRef workflow.Workflow
	c.Assert(wf, FitsTypeOf, &wfRef)

	var exit = false
	var rx bool
	ctxOut, _ := context.WithTimeout(context.Background(), 6000*time.Millisecond)
	for run := true; run; run = !exit {
		select {
		case <-e.GetResponseChannel():
			rx = true
			exit = true
			break

		case <-ctxOut.Done():
			fmt.Println("Timeout")
			exit = true
		}
	}

	c.Assert(rx, Equals, true)
}

func (s *EngineSuite) TestStartSyncWorkflow(c *C) {
	getWorkflowInstanceFn := func(ctx context.Context, name string, engine *engine.Engine) interfaces.IWorkflow {
		return createGoodSyncWorkflowInstance(engine, name)
	}
	e := engine.NewEngine(getChannels(), "if", getWorkflowInstanceFn, &log.Logger{})

	ctx, _ := context.WithCancel(context.Background())
	e.Start()
	e.StartWorkflow(ctx, "WfTest")
	wf := e.GetWorkflowInstance(ctx, "WfTest")
	var wfRef workflow.Workflow
	c.Assert(wf, FitsTypeOf, &wfRef)

	cnt := 0
	var exit = false
	var rx bool
	ctxOut, _ := context.WithTimeout(context.Background(), 5000*time.Millisecond)
	for run := true; run; run = !exit {
		select {
		case r := <-e.GetResponseChannel():
			resp := r.(workflow.Response)
			if resp.Code == interfaces.OK {
				cnt++
				if cnt == 5 {
					e.Stop()
					rx = true
					exit = true
				}
			}
			break

		case <-ctxOut.Done():
			fmt.Println("Timeout")
			exit = true
		}
	}
	c.Assert(rx, Equals, true)
}

func (s *EngineSuite) TestStartAsyncWorkflow_AlreadyRunning(c *C) {
	getWorkflowInstanceFn := func(ctx context.Context, name string, engine *engine.Engine) interfaces.IWorkflow {
		return createGoodAsyncWorkflowInstance(engine, name)
	}
	e := engine.NewEngine(getChannels(), "if", getWorkflowInstanceFn, &log.Logger{})

	ctx, _ := context.WithCancel(context.Background())

	e.Start()

	e.StartWorkflow(ctx, "goodWorkflow")

	time.Sleep(250 * time.Millisecond)

	e.StartWorkflow(ctx, "goodWorkflow")

	fmt.Println("Test completed")

	var rx bool
	ctxOut, _ := context.WithTimeout(context.Background(), 5000*time.Millisecond)
	for {
		select {
		case r := <-e.GetResponseChannel():
			resp := r.(workflow.Response)
			if resp.Result == interfaces.OK {
				rx = true
				break
			}

		case <-ctxOut.Done():
			fmt.Println("Timeout")
			rx = false
			return
		}
	}
	c.Assert(rx, Equals, true)
}

func (s *EngineSuite) TestStart_DoubleWorkflowInstance_Sync(c *C) {
	getWorkflowInstanceFn := func(ctx context.Context, name string, engine *engine.Engine) interfaces.IWorkflow {
		if name == "goodWorkflow" {
			return createGoodSyncWorkflowInstance(engine, name)
		} else {
			return createBadSyncWorkflowInstance(engine, name)
		}
	}
	e := engine.NewEngine(getChannels(), "if", getWorkflowInstanceFn, &log.Logger{})

	ctx, _ := context.WithCancel(context.Background())

	e.Start()

	e.StartWorkflow(ctx, "goodWorkflow")

	e.StartWorkflow(ctx, "badWorkflow")

	var exit = false
	var rx bool
	ctxOut, _ := context.WithTimeout(context.Background(), 5000*time.Millisecond)
	for run := true; run; run = !exit {
		select {
		case r := <-e.GetResponseChannel():
			resp := r.(workflow.Response)
			if resp.Code == interfaces.OK {
				break
			} else {
				e.Stop()
				rx = true
				break
			}

		case <-ctxOut.Done():
			fmt.Println("Timeout")
			exit = true
		}
	}

	c.Assert(rx, Equals, true)
}

func (s *EngineSuite) TestStart_DoubleWorkflowInstance_Async(c *C) {
	getWorkflowInstanceFn := func(ctx context.Context, name string, engine *engine.Engine) interfaces.IWorkflow {
		return createGoodAsyncWorkflowInstance(engine, name)
	}
	e := engine.NewEngine(getChannels(), "if", getWorkflowInstanceFn, &log.Logger{})

	ctx, _ := context.WithCancel(context.Background())

	e.Start()

	e.StartWorkflow(ctx, "goodWorkflow")

	e.StartWorkflow(ctx, "badWorkflow")

	var exit = false
	var rx bool
	ctxOut, _ := context.WithTimeout(context.Background(), 5000*time.Millisecond)
	for run := true; run; run = !exit {
		select {
		case <-e.GetResponseChannel():
			rx = true
			break

		case <-ctxOut.Done():
			fmt.Println("Timeout")
			exit = true
			return
		}
	}
	c.Assert(rx, Equals, true)
}

func (s *EngineSuite) TestStopEngine(c *C) {
	getWorkflowInstanceFn := func(ctx context.Context, name string, engine *engine.Engine) interfaces.IWorkflow {
		return createGoodAsyncWorkflowInstance(engine, name)
	}
	e := engine.NewEngine(getChannels(), "if", getWorkflowInstanceFn, &log.Logger{})

	ctx, _ := context.WithCancel(context.Background())
	e.Start()
	e.StartWorkflow(ctx, "WfTest")
	e.Stop()
}
