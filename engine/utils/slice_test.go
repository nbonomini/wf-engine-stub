package utils_test

import (
	"ftsystem.com/wf-engine-stub/engine/utils"
	. "gopkg.in/check.v1"
	"testing"
)

func TestSlice(t *testing.T) { TestingT(t) }

type UtilsSliceSuite struct{}

var _ = Suite(&UtilsSliceSuite{})

func (s *UtilsSliceSuite) TestIndexOf(c *C) {
	type args struct {
		element string
		data    []string
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "Find first",
			args: args{
				element: "first",
				data:    []string{"first", "second", "third"},
			},
			want: 0,
		},
		{
			name: "Find second",
			args: args{
				element: "second",
				data:    []string{"first", "second", "third"},
			},
			want: 1,
		},
		{
			name: "Find third",
			args: args{
				element: "third",
				data:    []string{"first", "second", "third"},
			},
			want: 2,
		},
		{
			name: "Find nothing",
			args: args{
				element: "nothing",
				data:    []string{"first", "second", "third"},
			},
			want: -1,
		},
	}
	for _, tt := range tests {
		c.Assert(utils.IndexOf(tt.args.element, tt.args.data), Equals, tt.want)
	}
}
