package log

import "fmt"

type Logger struct{}

func NewLogger() *Logger {
	return &Logger{}
}

func (l *Logger) Info(msg string) {
	fmt.Println("Info:", msg)
}

func (l *Logger) Warning(msg string) {
	fmt.Println("Warning:", msg)
}

func (l *Logger) Error(msg string) {
	fmt.Println("Error:", msg)
}

func (l *Logger) Debug(msg string) {
	fmt.Println("Debug:", msg)
}
