package log_test

import (
	"ftsystem.com/wf-engine-stub/engine/log"
	. "gopkg.in/check.v1"
	"testing"
)

func TestLog(t *testing.T) { TestingT(t) }

type LogSuite struct{}

var _ = Suite(&LogSuite{})

func (s *LogSuite) TestNewLogger(c *C) {
	var expectedLogger *log.Logger
	c.Assert(log.NewLogger(), FitsTypeOf, expectedLogger)
}

func (s *LogSuite) TestInfo(c *C) {
	logger := log.NewLogger()
	logger.Info("Message")
}

func (s *LogSuite) TestWarning(c *C) {
	logger := log.NewLogger()
	logger.Warning("Message")
}

func (s *LogSuite) TestError(c *C) {
	logger := log.NewLogger()
	logger.Error("Message")
}

func (s *LogSuite) TestDebug(c *C) {
	logger := log.NewLogger()
	logger.Debug("Message")
}
