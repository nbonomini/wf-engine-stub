package interfaces

type Status int

const (
	OK Status = iota
	KO
)

type IChannelRequest interface {
	Type() string
	ParameterList() []interface{}
}

type IChannelResponse interface {
	Code() Status
	Data() interface{}
	ErrorMessage() string
}

type IChannel interface {
	Send(IChannelRequest) IChannelResponse
}
