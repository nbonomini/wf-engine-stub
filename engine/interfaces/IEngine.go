package interfaces

type IEngine interface {
	GetResponseChannel() chan IWfResponse
	GetDeviceChannels() map[string]IChannel
}
