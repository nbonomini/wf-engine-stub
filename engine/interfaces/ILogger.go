package interfaces

type ILogger interface {
	Info(msg string)
	Warning(msg string)
	Error(msg string)
	Debug(msg string)
}
