package interfaces

type ICommandResponse interface {
	Code() Status
	Data() interface{}
	ErrorMessage() string
}

type ICommand interface {
	Execute() ICommandResponse
}
