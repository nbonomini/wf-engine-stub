package interfaces

type Triggers map[string]string

func (t Triggers) GetEventByState(state string) (string, bool) {
	event, exists := t[state]
	return event, exists
}

type IWorkflow interface {
	GetName() string
	GetCommandChannel() chan string
	IsRunning() bool
	IsCompleted() bool
	Run()
	Stop() error
	Reset() error
	GetState() string
	IsSync() bool
	TriggerEvent(event string) error
	GetFsm() interface{}
	SetFsm(interface{})
	GetTriggers() Triggers
	SetTriggers(Triggers)
}
