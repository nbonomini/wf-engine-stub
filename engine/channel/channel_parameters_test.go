package channel_test

import (
	"fmt"
	"ftsystem.com/wf-engine-stub/engine/channel"
	"google.golang.org/grpc"
	. "gopkg.in/check.v1"
	"testing"
)

func TestChannelParameters(t *testing.T) { TestingT(t) }

type ChannelParameterSuite struct{}

var _ = Suite(&ChannelParameterSuite{})

func (s *ChannelParameterSuite) TestNewChannelParameters(c *C) {
	channelParameters := channel.NewChannelParameters()
	c.Assert(channelParameters, DeepEquals, &channel.Parameters{
		Parameters: map[string]interface{}{},
	})
}

func (s *ChannelParameterSuite) TestFullAddress(c *C) {
	tcpParameters := &channel.TcpParameters{
		Ip:   "127.0.0.1",
		Port: 8080,
	}
	result := tcpParameters.FullAddress()
	expectedResult := "127.0.0.1:8080"
	c.Assert(result, Equals, expectedResult)
}

func ExampleTcpParameters_FullAddress() {
	tcpParameters := &channel.TcpParameters{
		Ip:   "127.0.0.1",
		Port: 8080,
	}
	fmt.Println(tcpParameters.FullAddress())
	// output: 127.0.0.1:8080
}

func (s *ChannelParameterSuite) TestSetAndGetGrpc(c *C) {
	channelParameters := channel.NewChannelParameters()
	type fakeConnClient struct{}
	channelParameters.SetGrpc("127.0.0.1", 8080, func(c grpc.ClientConnInterface) interface{} {
		return &fakeConnClient{}
	})
	c.Assert(channelParameters.Parameters["grpc"], NotNil)
	var grpcParameters channel.GrpcParameters
	c.Assert(channelParameters.Grpc(), FitsTypeOf, &grpcParameters)
	c.Assert(channelParameters.Channels(), DeepEquals, []string{"grpc"})
}

func (s *ChannelParameterSuite) TestSetAndGetOpcUa(c *C) {
	channelParameters := channel.NewChannelParameters()
	channelParameters.SetOpcUa("127.0.0.1", 8080)
	c.Assert(channelParameters.Parameters["opcua"], NotNil)
	var opcuaParameters channel.OpcUaParameters
	c.Assert(channelParameters.OpcUa(), FitsTypeOf, &opcuaParameters)
	c.Assert(channelParameters.Channels(), DeepEquals, []string{"opcua"})
}

func (s *ChannelParameterSuite) TestSetAndGetModbusTcp(c *C) {
	channelParameters := channel.NewChannelParameters()
	channelParameters.SetModbusTcp("127.0.0.1", 8080, 2000)
	c.Assert(channelParameters.Parameters["modbustcp"], NotNil)
	var tcpParameters channel.TcpParameters
	c.Assert(channelParameters.ModbusTcp(), FitsTypeOf, &tcpParameters)
	c.Assert(channelParameters.Channels(), DeepEquals, []string{"modbustcp"})
}

func (s *ChannelParameterSuite) TestChannelsGetter(c *C) {
	channelParameters := channel.NewChannelParameters()
	type fakeConnClient struct{}
	channelParameters.SetGrpc("127.0.0.1", 8080, func(c grpc.ClientConnInterface) interface{} {
		return &fakeConnClient{}
	})
	c.Assert(channelParameters.Channels(), DeepEquals, []string{"grpc"})

	channelParameters.SetModbusTcp("127.0.0.1", 8080, 2000)
	c.Assert(unsortedEqual(channelParameters.Channels(), []string{"grpc", "modbustcp"}), Equals, true)

	channelParameters.SetOpcUa("127.0.0.1", 8080)
	c.Assert(unsortedEqual(channelParameters.Channels(), []string{"grpc", "modbustcp", "opcua"}), Equals, true)
}

func unsortedEqual(a []string, b []string) bool {
	if len(a) != len(b) {
		return false
	}
	for _, aItem := range a {
		bContainsAItem := false
		for _, bItem := range b {
			if aItem == bItem {
				bContainsAItem = true
				break
			}
		}
		if !bContainsAItem {
			return false
		}
	}
	return true
}
