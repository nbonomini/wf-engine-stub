package channel

import "ftsystem.com/wf-engine-stub/engine/interfaces"

type Response struct {
	code         interfaces.Status
	data         interface{}
	errorMessage string
}

func NewResponse(code interfaces.Status, data interface{}, errorMessage string) *Response {
	return &Response{
		code:         code,
		data:         data,
		errorMessage: errorMessage,
	}
}

func (r *Response) Code() interfaces.Status {
	return r.code
}

func (r *Response) Data() interface{} {
	return r.data
}

func (r *Response) ErrorMessage() string {
	return r.errorMessage
}
