package channel

type Request struct {
	channelType   string
	parameterList []interface{}
}

func NewRequest(channelType string, parameterList []interface{}) *Request {
	return &Request{
		channelType:   channelType,
		parameterList: parameterList,
	}
}

func (r *Request) Type() string {
	return r.channelType
}

func (r *Request) ParameterList() []interface{} {
	return r.parameterList
}
