package channel

import (
	"ftsystem.com/wf-engine-stub/engine/interfaces"
	"google.golang.org/grpc"
	"reflect"
)

// GrpcChannel describes a communication channel through grpc protocol
type GrpcChannel struct {
	Address string
	Client  interface{}
}

// GetGrpcClient is a function that receive a connection and return a grpc client
type GetGrpcClient func(connInterface grpc.ClientConnInterface) interface{}

// NewGrpcChannel creates a GrpcChannel instance connected to the grpc server
func NewGrpcChannel(address string, fn GetGrpcClient) (*GrpcChannel, error) {
	conn, err := grpc.Dial(address, grpc.WithInsecure())
	if err != nil {
		return nil, err
	}

	client := fn(conn)

	return &GrpcChannel{
		Address: address,
		Client:  client,
	}, nil
}

// Send make a call to a grpc service specified inside the request give as argument
func (c *GrpcChannel) Send(request interfaces.IChannelRequest) interfaces.IChannelResponse {
	method := reflect.ValueOf(c.Client).MethodByName(request.Type())
	var inputs []reflect.Value
	for _, param := range request.ParameterList() {
		inputs = append(inputs, reflect.ValueOf(param))
	}
	returns := method.Call(inputs)
	data := returns[0].Interface()
	err := returns[1].Interface()

	if err != nil {
		resultError := err.(error)
		return NewResponse(interfaces.KO, nil, resultError.Error())
	}

	return NewResponse(interfaces.OK, data, "")
}
