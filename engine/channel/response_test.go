package channel_test

import (
	"ftsystem.com/wf-engine-stub/engine/channel"
	"ftsystem.com/wf-engine-stub/engine/interfaces"
	. "gopkg.in/check.v1"
	"testing"
)

func TestResponse(t *testing.T) { TestingT(t) }

type ResponseSuite struct{}

var _ = Suite(&ResponseSuite{})

func (s *ResponseSuite) TestNewResponse(c *C) {
	response := channel.NewResponse(interfaces.OK, "Test", "")
	var expectedResponse *channel.Response
	c.Assert(response, FitsTypeOf, expectedResponse)
}

func (s *ResponseSuite) TestCode(c *C) {
	response := channel.NewResponse(interfaces.OK, "Test", "")
	code := response.Code()
	c.Assert(code, Equals, interfaces.OK)
}

func (s *ResponseSuite) TestData(c *C) {
	response := channel.NewResponse(interfaces.OK, "Test", "")
	data := response.Data()
	c.Assert(data, Equals, "Test")
}

func (s *ResponseSuite) TestErrorMessage(c *C) {
	response := channel.NewResponse(interfaces.OK, "Test", "My error message")
	errorMessage := response.ErrorMessage()
	c.Assert(errorMessage, Equals, "My error message")
}
