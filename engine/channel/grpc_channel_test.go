package channel_test

import (
	"bou.ke/monkey"
	"errors"
	"ftsystem.com/wf-engine-stub/engine/channel"
	"google.golang.org/grpc"
	. "gopkg.in/check.v1"
	"testing"
)

func TestGrpcChannel(t *testing.T) { TestingT(t) }

type GrpcChannelSuite struct{}

var _ = Suite(&GrpcChannelSuite{})

func (s *GrpcChannelSuite) TestNewGrpcChannel(c *C) {
	defer monkey.UnpatchAll()

	// failure case
	monkey.Patch(grpc.Dial, func(target string, opts ...grpc.DialOption) (*grpc.ClientConn, error) {
		err := errors.New("connection error")
		return nil, err
	})

	fn := func(connInterface grpc.ClientConnInterface) interface{} {
		return ""
	}
	grpcChannel, err := channel.NewGrpcChannel("127.0.0.1:8080", fn)
	c.Assert(grpcChannel, IsNil)
	c.Assert(err, NotNil)

	// successful case
	monkey.Patch(grpc.Dial, func(target string, opts ...grpc.DialOption) (*grpc.ClientConn, error) {
		conn := &grpc.ClientConn{}
		return conn, nil
	})
	grpcChannel, err = channel.NewGrpcChannel("127.0.0.1:8080", fn)
	var expectedGrpcChannel *channel.GrpcChannel
	c.Assert(err, IsNil)
	c.Assert(grpcChannel, FitsTypeOf, expectedGrpcChannel)
}

type fakeClient struct{}

func (c *fakeClient) FakeSuccessfulMethod(msg string) (string, error) {
	return msg, nil
}

func (c *fakeClient) FakeFailureMethod() (string, error) {
	fakeError := errors.New("fake error")
	return "", fakeError
}

func (s *GrpcChannelSuite) TestSend(c *C) {
	fakeClientInstance := &fakeClient{}
	fakeParameter := "Fake response"
	fn := func(connInterface grpc.ClientConnInterface) interface{} {
		return fakeClientInstance
	}
	grpcChannel, _ := channel.NewGrpcChannel("127.0.0.1:8080", fn)
	parameterList := []interface{}{fakeParameter}

	// success
	request := channel.NewRequest(
		"FakeSuccessfulMethod",
		parameterList,
	)
	response := grpcChannel.Send(request)
	c.Assert(response.Data(), Equals, fakeParameter)

	// error
	request = channel.NewRequest(
		"FakeFailureMethod",
		[]interface{}{},
	)
	response = grpcChannel.Send(request)
	c.Assert(response.ErrorMessage(), Equals, "fake error")
}
