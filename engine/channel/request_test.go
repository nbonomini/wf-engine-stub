package channel_test

import (
	"ftsystem.com/wf-engine-stub/engine/channel"
	. "gopkg.in/check.v1"
	"testing"
)

func TestRequest(t *testing.T) { TestingT(t) }

type RequestSuite struct{}

var _ = Suite(&RequestSuite{})

func (s *RequestSuite) TestNewRequest(c *C) {
	var parameters []interface{}
	request := channel.NewRequest("grpc", parameters)
	var expectedRequest *channel.Request
	c.Assert(request, FitsTypeOf, expectedRequest)
}

func (s *RequestSuite) TestType(c *C) {
	var parameters []interface{}
	request := channel.NewRequest("grpc", parameters)
	requestType := request.Type()
	c.Assert(requestType, Equals, "grpc")
}

func (s *RequestSuite) TestParameterList(c *C) {
	var parameters []interface{}
	request := channel.NewRequest("grpc", parameters)
	parameterList := request.ParameterList()
	c.Assert(parameterList, DeepEquals, parameters)
}
