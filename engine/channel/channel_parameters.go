// Package channel contains all channel utilities to create all kind of communication with external devices
package channel

import (
	"fmt"
)

// TcpParameters describes which parameters should have a TCP communication
type TcpParameters struct {
	Ip      string
	Port    int
	Timeout int
}

// FullAddress builds a tcp communication address from Ip and Port properties
func (p *TcpParameters) FullAddress() string {
	return fmt.Sprintf("%s:%d", p.Ip, p.Port)
}

// GrpcParameters describes grpc communication required parameters
type GrpcParameters struct {
	TcpParameters
	ClientInstanceFn GetGrpcClient
}

// OpcUaParameters describes OpcUA communication required parameters
type OpcUaParameters struct {
	TcpParameters
}

// Parameters is a generic struct with a map of parameters as property
type Parameters struct {
	Parameters map[string]interface{}
}

// NewChannelParameters creates a Parameters instance with an empty parameters map
func NewChannelParameters() *Parameters {
	return &Parameters{
		Parameters: map[string]interface{}{},
	}
}

// SetGrpc sets a grpc parameters instance in the Parameters map
func (e *Parameters) SetGrpc(ip string, port int, clientInstanceFn GetGrpcClient) {
	e.Parameters["grpc"] = &GrpcParameters{TcpParameters: TcpParameters{Ip: ip, Port: port}, ClientInstanceFn: clientInstanceFn}
}

// SetOpcUa sets an OpcUA parameters instance in the Parameters map
func (e *Parameters) SetOpcUa(ip string, port int) {
	e.Parameters["opcua"] = &OpcUaParameters{TcpParameters: TcpParameters{Ip: ip, Port: port}}
}

// SetModbusTcp sets a ModBus TCP parameters instance in the Parameters map
func (e *Parameters) SetModbusTcp(ip string, port int, timeout int) {
	e.Parameters["modbustcp"] = &TcpParameters{Ip: ip, Port: port, Timeout: timeout}
}

// Grpc returns the grpc parameters instance
func (e *Parameters) Grpc() interface{} {
	return e.Parameters["grpc"]
}

// ModbusTcp returns the modbus TCP parameters instance
func (e *Parameters) ModbusTcp() interface{} {
	return e.Parameters["modbustcp"]
}

// OpcUa returns the OpcUa parameters instance
func (e *Parameters) OpcUa() interface{} {
	return e.Parameters["opcua"]
}

// Channels returns the list of channel keys
func (e *Parameters) Channels() []string {
	keys := make([]string, 0, len(e.Parameters))
	for key := range e.Parameters {
		keys = append(keys, key)
	}
	return keys
}
