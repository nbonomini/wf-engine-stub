package workflow_test

import (
	"context"
	"errors"
	"ftsystem.com/wf-engine-stub/engine/command"
	"ftsystem.com/wf-engine-stub/engine/engine"
	"ftsystem.com/wf-engine-stub/engine/interfaces"
	"ftsystem.com/wf-engine-stub/engine/log"
	"ftsystem.com/wf-engine-stub/engine/workflow"
	"github.com/looplab/fsm"
	. "gopkg.in/check.v1"
	"testing"
)

func TestWorkflow(t *testing.T) { TestingT(t) }

type WorkflowSuite struct{}

var _ = Suite(&WorkflowSuite{})

func createFakeEngine() *engine.Engine {
	channels := map[string]interfaces.IChannel{}
	getWorkflowInstanceFn := func(ctx context.Context, name string, engine *engine.Engine) interfaces.IWorkflow {
		return createMyWorkflowInstance(engine, name)
	}
	return engine.NewEngine(channels, "randomDeviceKey", getWorkflowInstanceFn, &log.Logger{})
}

func createMyWorkflowInstance(engine *engine.Engine, name string) interfaces.IWorkflow {
	w := workflow.NewSyncWorkflow(engine, name)

	w.FSM = fsm.NewFSM(
		"idle",
		fsm.Events{
			{Name: "init", Src: []string{"idle"}, Dst: "initializing"},
			{Name: "start", Src: []string{"initializing"}, Dst: "started"},
			{Name: "stop", Src: []string{"started"}, Dst: "idle"},
			{Name: "complete", Src: []string{"started"}, Dst: "completed"},
			{Name: "reset", Src: []string{"completed"}, Dst: "idle"},
		},
		fsm.Callbacks{
			"after_start": func(e *fsm.Event) {},
		},
	)

	w.Triggers = interfaces.Triggers{
		"initializing": "start",
		"started":      "complete",
	}

	return w
}

func (s *WorkflowSuite) TestNewWorkflow(c *C) {
	engineInstance := createFakeEngine()
	myWorkflow := workflow.NewWorkflow(engineInstance, "myWorkflow", workflow.Async)
	var workflowInstance *workflow.Workflow
	c.Assert(myWorkflow, FitsTypeOf, workflowInstance)
}

func (s *WorkflowSuite) TestNewAsyncWorkflow(c *C) {
	engineInstance := createFakeEngine()
	myWorkflow := workflow.NewAsyncWorkflow(engineInstance, "myWorkflow")
	var workflowInstance *workflow.Workflow
	c.Assert(myWorkflow, FitsTypeOf, workflowInstance)
	c.Assert(myWorkflow.Type, Equals, workflow.Async)
}

func (s *WorkflowSuite) TestNewSyncWorkflow(c *C) {
	engineInstance := createFakeEngine()
	myWorkflow := workflow.NewSyncWorkflow(engineInstance, "myWorkflow")
	var workflowInstance *workflow.Workflow
	c.Assert(myWorkflow, FitsTypeOf, workflowInstance)
	c.Assert(myWorkflow.Type, Equals, workflow.Sync)
}

func (s *WorkflowSuite) TestGetName(c *C) {
	engineInstance := createFakeEngine()
	wfName := "myWorkflow"
	myWorkflow := workflow.NewSyncWorkflow(engineInstance, wfName)
	c.Assert(myWorkflow.GetName(), Equals, wfName)
}

func (s *WorkflowSuite) TestGetCommandChannel(c *C) {
	engineInstance := createFakeEngine()
	myWorkflow := workflow.NewSyncWorkflow(engineInstance, "myWorkflow")
	var cmdChannel chan string
	c.Assert(myWorkflow.GetCommandChannel(), FitsTypeOf, cmdChannel)
}

func (s *WorkflowSuite) TestIsRunning(c *C) {
	ctx := context.Background()
	engineInstance := createFakeEngine()
	myWorkflow := engineInstance.GetWorkflowInstance(ctx, "myWorkflow")
	c.Assert(myWorkflow.IsRunning(), Equals, false)
	myWorkflow.Run()
	c.Assert(myWorkflow.IsRunning(), Equals, true)
	_ = myWorkflow.Stop()
	c.Assert(myWorkflow.IsRunning(), Equals, false)
}

func (s *WorkflowSuite) TestIsCompleted(c *C) {
	ctx := context.Background()
	engineInstance := createFakeEngine()
	myWf := engineInstance.GetWorkflowInstance(ctx, "myWorkflow")
	c.Assert(myWf.IsCompleted(), Equals, false)
	myWf.Run()
	c.Assert(myWf.IsCompleted(), Equals, false)
	_ = myWf.TriggerEvent("complete")
	c.Assert(myWf.IsCompleted(), Equals, true)
}

func (s *WorkflowSuite) TestSendError(c *C) {
	engineInstance := createFakeEngine()
	myWorkflow := workflow.NewSyncWorkflow(engineInstance, "myWorkflow")
	myError := errors.New("my error")
	responseChannel := engineInstance.GetResponseChannel()
	myWorkflow.SendError(myError)
	engineResponse := <-responseChannel
	c.Assert(engineResponse.(workflow.Response).Code, Equals, interfaces.KO)
	c.Assert(engineResponse.(workflow.Response).ErrorMessage, Equals, "my error")
}

func (s *WorkflowSuite) TestSendResponse(c *C) {
	engineInstance := createFakeEngine()
	myWorkflow := workflow.NewSyncWorkflow(engineInstance, "myWorkflow")
	responseChannel := engineInstance.GetResponseChannel()
	response := command.NewResponse(interfaces.OK, "my result", "")
	myWorkflow.SendResponse(response)
	engineResponse := <-responseChannel
	c.Assert(engineResponse.(workflow.Response).Code, Equals, interfaces.OK)
	c.Assert(engineResponse.(workflow.Response).Result, Equals, "my result")
}

func (s *WorkflowSuite) TestGetState(c *C) {
	ctx := context.Background()
	engineInstance := createFakeEngine()
	myWf := engineInstance.GetWorkflowInstance(ctx, "myWorkflow")
	c.Assert(myWf.GetState(), Equals, "idle")
	myWf.Run()
	c.Assert(myWf.GetState(), Equals, "started")
	_ = myWf.Stop()
	c.Assert(myWf.GetState(), Equals, "idle")
	myWf.Run()
	_ = myWf.TriggerEvent("complete")
	c.Assert(myWf.GetState(), Equals, "completed")
	_ = myWf.Reset()
	c.Assert(myWf.GetState(), Equals, "idle")
}

func (s *WorkflowSuite) TestIsSync(c *C) {
	engineInstance := createFakeEngine()
	syncWorkflow := workflow.NewSyncWorkflow(engineInstance, "myWorkflow")
	c.Assert(syncWorkflow.IsSync(), Equals, true)
	asyncWorkflow := workflow.NewAsyncWorkflow(engineInstance, "myWorkflow")
	c.Assert(asyncWorkflow.IsSync(), Equals, false)
}

func (s *WorkflowSuite) TestResetError(c *C) {
	ctx := context.Background()
	engineInstance := createFakeEngine()
	myWf := engineInstance.GetWorkflowInstance(ctx, "myWorkflow")
	// reset should be triggered only from "completed" state
	err := myWf.Reset()
	c.Assert(err, NotNil)
}
