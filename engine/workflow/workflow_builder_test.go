package workflow_test

import (
	"fmt"
	"ftsystem.com/wf-engine-stub/engine/workflow"
	"github.com/looplab/fsm"
	. "gopkg.in/check.v1"
	"testing"
)

// Utilities

func countIfExists(list []string, target string) int {
	counter := 0
	for _, item := range list {
		if item == target {
			counter++
		}
	}
	return counter
}

// Tests

func TestWorkflowBuilder(t *testing.T) { TestingT(t) }

type WorkflowBuilderSuite struct{}

var _ = Suite(&WorkflowBuilderSuite{})

func (s *WorkflowBuilderSuite) TestNewWorkflowBuilder(c *C) {
	builder := workflow.NewWorkflowBuilder(workflow.Cyclic)
	var builderInstance *workflow.Builder
	c.Assert(builder, FitsTypeOf, builderInstance)
}

func (s *WorkflowBuilderSuite) TestWorkflowBuilder_HasEvent(c *C) {
	builder := workflow.NewWorkflowBuilder(workflow.Cyclic)
	event := fsm.EventDesc{Name: "execute", Src: []string{}, Dst: "completed"}
	c.Assert(builder.HasEvent(event), Equals, false)
	builder.AddEvent(event)
	c.Assert(builder.IsValid(), Equals, true)
	c.Assert(builder.HasEvent(event), Equals, true)
}

func (s *WorkflowBuilderSuite) TestWorkflowBuilder_AddEvent(c *C) {
	builder := workflow.NewWorkflowBuilder(workflow.Cyclic)
	beforeEventCount := len(builder.Events())
	builder.AddEvent(fsm.EventDesc{Name: "execute", Src: []string{"initialized"}, Dst: "newState"})
	c.Assert(builder.IsValid(), Equals, true)
	afterEventCount := len(builder.Events())
	// builder should have a unique list of default states and the new one
	c.Assert(builder.States(), DeepEquals, []string{"idle", "completed", "initialized", "newState"})
	c.Assert(afterEventCount, Equals, beforeEventCount+1)

	// add existing event
	builder.AddEvent(fsm.EventDesc{Name: "execute", Src: []string{"initialized"}, Dst: "newState"})
	c.Assert(builder.IsValid(), Equals, false)
}

func (s *WorkflowBuilderSuite) TestWorkflowBuilder_AddUniqueState(c *C) {
	builder := workflow.NewWorkflowBuilder(workflow.Cyclic)
	existingResult := builder.AddUniqueState("completed")
	c.Assert(existingResult, Equals, false)
	newResult := builder.AddUniqueState("newState")
	c.Assert(newResult, Equals, true)
	repeatedResult := builder.AddUniqueState("newState")
	c.Assert(repeatedResult, Equals, false)
	c.Assert(countIfExists(builder.States(), "newState"), Equals, 1)
}

func (s *WorkflowBuilderSuite) TestWorkflowBuilder_AddTrigger(c *C) {
	builder := workflow.NewWorkflowBuilder(workflow.Cyclic)
	builder.AddTrigger("initialized", "stop")
	c.Assert(builder.IsValid(), Equals, true)

	c.Assert(builder.Triggers()["initialized"], Equals, "stop")
	builder.AddTrigger("initialized", "notExistingEvent")
	c.Assert(builder.IsValid(), Equals, false)

	builder.AddTrigger("notExistingState", "stop")
	c.Assert(builder.IsValid(), Equals, false)
}

func (s *WorkflowBuilderSuite) TestWorkflowBuilder_AfterEventCallback(c *C) {
	builder := workflow.NewWorkflowBuilder(workflow.Cyclic)
	builder.AfterEventCallback("stop", func(e *fsm.Event) {})
	c.Assert(builder.IsValid(), Equals, true)

	callbacks := builder.Callbacks()
	_, exists := callbacks["after_stop"]
	c.Assert(exists, Equals, true)

	// testing missing event
	builder.AfterEventCallback("missingEvent", func(e *fsm.Event) {})
	c.Assert(builder.IsValid(), Equals, false)

	callbacks = builder.Callbacks()
	_, exists = callbacks["after_missingEvent"]
	c.Assert(exists, Equals, false)
}

func (s *WorkflowBuilderSuite) TestWorkflowBuilder_BeforeEventCallback(c *C) {
	builder := workflow.NewWorkflowBuilder(workflow.Cyclic)
	builder.BeforeEventCallback("stop", func(e *fsm.Event) {})
	c.Assert(builder.IsValid(), Equals, true)

	callbacks := builder.Callbacks()
	_, exists := callbacks["before_stop"]
	c.Assert(exists, Equals, true)
}

func (s *WorkflowBuilderSuite) TestWorkflowBuilder_BeforeAllEventsCallback(c *C) {
	builder := workflow.NewWorkflowBuilder(workflow.Cyclic)
	builder.BeforeAllEventsCallback(func(e *fsm.Event) {})
	callbacks := builder.Callbacks()
	_, exists := callbacks["before_event"]
	c.Assert(exists, Equals, true)
}

func (s *WorkflowBuilderSuite) TestWorkflowBuilder_AfterAllEventsCallback(c *C) {
	builder := workflow.NewWorkflowBuilder(workflow.Cyclic)
	builder.AfterAllEventsCallback(func(e *fsm.Event) {})
	callbacks := builder.Callbacks()
	_, exists := callbacks["after_event"]
	c.Assert(exists, Equals, true)
}

func (s *WorkflowBuilderSuite) TestWorkflowBuilder_EnterStateCallback(c *C) {
	builder := workflow.NewWorkflowBuilder(workflow.Cyclic)
	builder.EnterStateCallback("completed", func(e *fsm.Event) {})
	c.Assert(builder.IsValid(), Equals, true)

	callbacks := builder.Callbacks()
	_, exists := callbacks["enter_completed"]
	c.Assert(exists, Equals, true)

	// testing missing state
	builder.EnterStateCallback("missingState", func(e *fsm.Event) {})
	c.Assert(builder.IsValid(), Equals, false)

	callbacks = builder.Callbacks()
	_, exists = callbacks["enter_missingState"]
	c.Assert(exists, Equals, false)
}

func (s *WorkflowBuilderSuite) TestWorkflowBuilder_LeaveStateCallback(c *C) {
	builder := workflow.NewWorkflowBuilder(workflow.Cyclic)
	builder.LeaveStateCallback("completed", func(e *fsm.Event) {})
	c.Assert(builder.IsValid(), Equals, true)

	callbacks := builder.Callbacks()
	_, exists := callbacks["leave_completed"]
	c.Assert(exists, Equals, true)
}

func (s *WorkflowBuilderSuite) TestWorkflowBuilder_EnterAllStatesCallback(c *C) {
	builder := workflow.NewWorkflowBuilder(workflow.Cyclic)
	builder.EnterAllStatesCallback(func(e *fsm.Event) {})
	callbacks := builder.Callbacks()
	_, exists := callbacks["enter_state"]
	c.Assert(exists, Equals, true)
}

func (s *WorkflowBuilderSuite) TestWorkflowBuilder_LeaveAllStatesCallback(c *C) {
	builder := workflow.NewWorkflowBuilder(workflow.Cyclic)
	builder.LeaveAllStatesCallback(func(e *fsm.Event) {})
	callbacks := builder.Callbacks()
	_, exists := callbacks["leave_state"]
	c.Assert(exists, Equals, true)
}

func (s *WorkflowBuilderSuite) TestWorkflowBuilder_Build(c *C) {
	e := createFakeEngine()
	builder := workflow.NewWorkflowBuilder(workflow.Finite)

	wf, errors := builder.StartWith(&workflow.StartingEvent{
		Name: "connect",
		To:   "connected",
	}).
		AddEvent(fsm.EventDesc{
			Name: "execute",
			Src:  []string{"connected"},
			Dst:  "executed",
		}).
		AddTrigger("initialized", "connect").
		AddTrigger("connected", "execute").
		AddTrigger("executed", "stop").
		AfterEventCallback("execute", func(event *fsm.Event) {}).
		Build(e, workflow.Sync, "testWorkflow")

	c.Assert(errors, IsNil)

	// check event
	c.Assert(wf, NotNil)
	fms := wf.GetFsm().(*fsm.FSM)
	fms.SetState("initialized")
	fmt.Println("TRANSITION", fms.AvailableTransitions())
	c.Assert(countIfExists(fms.AvailableTransitions(), "connect"), Equals, 1)

	// check trigger
	triggers := wf.GetTriggers()
	_, exists := triggers.GetEventByState("initialized")
	c.Assert(exists, Equals, true)

	wf, errors = builder.
		Build(e, workflow.Async, "asyncWorkflow")

	wf, errors = builder.
		AddTrigger("notExistingState", "anotherEvent").
		Build(e, workflow.Sync, "wrongWorkflow")

	c.Assert(len(errors) > 0, Equals, true)

}

func (s *WorkflowBuilderSuite) TestWorkflowBuilder_Build_Panic(c *C) {
	e := createFakeEngine()
	builder := workflow.NewWorkflowBuilder(workflow.Cyclic)

	builder.
		StartWith(&workflow.StartingEvent{
			Name: "execute",
			To:   "executed",
		}).
		AddTrigger("initialized", "execute").
		AfterEventCallback("execute", func(event *fsm.Event) {})

	c.Assert(func() {
		builder.Build(e, 3, "panicWorkflow")
	}, PanicMatches, `Workflow Type '3' not implemented`)

}

func (s *WorkflowBuilderSuite) TestWorkflowBuilder_EndWhen(c *C) {

	e := createFakeEngine()
	builder := workflow.NewWorkflowBuilder(workflow.Finite)

	wf, errors := builder.StartWith(&workflow.StartingEvent{
		Name: "connect",
		To:   "connected",
	}).
		RegisterEvent(&workflow.Event{
			From: "connected",
			Name: "execute",
			To:   "executed",
		}).
		RegisterEvent(&workflow.Event{
			From: "executed",
			Name: "disconnect",
			To:   "disconnected",
		}).
		EndWhen("disconnected").
		AfterEventCallback("execute", func(event *fsm.Event) {}).
		Build(e, workflow.Sync, "acyclicSyncWorkflow")

	c.Assert(errors, IsNil)

	// check event
	c.Assert(wf, NotNil)
	fms := wf.GetFsm().(*fsm.FSM)
	fms.SetState("initialized")
	c.Assert(countIfExists(fms.AvailableTransitions(), "connect"), Equals, 1)

	// check trigger
	triggers := wf.GetTriggers()
	_, exists := triggers.GetEventByState("initialized")
	c.Assert(exists, Equals, true)
}

func (s *WorkflowBuilderSuite) TestWorkflowBuilder_ResumeWith(c *C) {
	e := createFakeEngine()
	builder := workflow.NewWorkflowBuilder(workflow.Cyclic)

	_, errors := builder.StartWith(&workflow.StartingEvent{
		Name: "connect",
		To:   "connected",
	}).
		RegisterEvent(&workflow.Event{
			From: "connected",
			Name: "execute",
			To:   "executed",
		}).
		ResumeWith(&workflow.ResumingEvent{
			From: "executed",
			Name: "connect",
		}).
		AfterEventCallback("execute", func(event *fsm.Event) {}).
		Build(e, workflow.Sync, "cyclicSyncWorkflow")

	c.Assert(errors, IsNil)
}
