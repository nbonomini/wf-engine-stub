package workflow

import (
	"errors"
	"fmt"
	i "ftsystem.com/wf-engine-stub/engine/interfaces"
	"ftsystem.com/wf-engine-stub/engine/utils"
	"github.com/looplab/fsm"
)

const DefaultInitialMachineState = "idle"

var defaultEvents = fsm.Events{
	{Name: "init", Src: []string{"idle", "completed"}, Dst: "initialized"},
	{Name: "stop", Src: []string{}, Dst: "completed"},
	{Name: "reset", Src: []string{"completed"}, Dst: "idle"},
	{Name: "error", Src: []string{}, Dst: "idle"},
}

// Builder use the Builder Pattern to help creating a workflow
type Builder struct {
	workflow      i.IWorkflow
	fsm           *fsm.FSM
	events        fsm.Events
	states        []string
	triggers      i.Triggers
	callbacks     fsm.Callbacks
	errors        []error
	recursion     Recursion
	startingEvent *StartingEvent
}

func NewWorkflowBuilder(recursion Recursion) *Builder {
	defaultStates := getStateFromEvents(defaultEvents)
	return &Builder{
		events:    defaultEvents,
		states:    defaultStates,
		triggers:  i.Triggers{},
		callbacks: fsm.Callbacks{},
		errors:    []error{},
		recursion: recursion,
	}
}

func (b *Builder) Build(engine i.IEngine, workflowType int, name string) (i.IWorkflow, []error) {

	b.Validate()

	if !b.IsValid() {
		return nil, b.Errors()
	}

	var wf i.IWorkflow
	if workflowType == Async {
		wf = NewAsyncWorkflow(engine, name)
	} else if workflowType == Sync {
		wf = NewSyncWorkflow(engine, name)
	} else {
		panic(fmt.Sprintf("Workflow Type '%d' not implemented", workflowType))
	}

	b.addAllStateToEventSourceState([]string{"stop", "error"})

	f := fsm.NewFSM(
		DefaultInitialMachineState,
		b.events,
		b.callbacks,
	)

	wf.SetFsm(f)

	wf.SetTriggers(b.triggers)

	return wf, nil
}

func (b *Builder) Validate() *Builder {
	if b.startingEvent == nil {
		b.AddError(errors.New("the starting event is not defined: please call StartWith method"))
	}
	return b
}

func (b *Builder) States() []string {
	return b.states
}

func (b *Builder) Events() fsm.Events {
	return b.events
}

func (b *Builder) RegisterEvent(event *Event) *Builder {
	b.AddEvent(fsm.EventDesc{
		Src:  []string{event.From},
		Name: event.Name,
		Dst:  event.To,
	})
	b.AddTrigger(event.From, event.Name)
	return b
}

func (b *Builder) StartWith(event *StartingEvent) *Builder {
	b.RegisterEvent(&Event{
		From: "initialized",
		Name: event.Name,
		To:   event.To,
	})
	b.startingEvent = event
	return b
}

func (b *Builder) StopWhen(from string) *Builder {
	b.AddTrigger(from, "stop")
	return b
}

func (b *Builder) ResumeWith(event *ResumingEvent) *Builder {
	b.appendSourceStateToEvent("start", event.From)
	b.AddTrigger(event.From, event.Name)
	return b
}

func (b *Builder) EndWhen(from string) *Builder {
	if b.recursion == Cyclic {
		b.ResumeWith(&ResumingEvent{
			From: from,
			Name: b.startingEvent.Name,
		})
	} else {
		b.StopWhen(from)
	}
	return b
}

func (b *Builder) AddEvent(event fsm.EventDesc) *Builder {
	if b.HasEvent(event) {
		b.AddError(errors.New("event already declared"))
	}
	b.events = append(b.events, event)
	b.addStateFromEvent(event)
	return b
}

func (b *Builder) AddError(err error) {
	b.errors = append(b.errors, err)
}

func (b *Builder) Errors() []error {
	return b.errors
}

func (b *Builder) HasEvent(event fsm.EventDesc) bool {
	return b.HasEventByName(event.Name)
}

func (b *Builder) HasEventByName(eventName string) bool {
	for _, existingEvent := range b.events {
		if existingEvent.Name == eventName {
			return true
		}
	}
	return false
}

func (b *Builder) AddUniqueState(state string) bool {
	if b.HasState(state) {
		return false
	}
	b.states = append(b.states, state)
	return true
}

func (b *Builder) HasState(state string) bool {
	for _, existingState := range b.states {
		if existingState == state {
			return true
		}
	}
	return false
}

func (b *Builder) AddTrigger(state string, eventName string) *Builder {
	if !b.HasState(state) {
		b.AddError(errors.New(fmt.Sprintf("state '%s' does not exists", state)))
	}

	if !b.HasEventByName(eventName) {
		b.AddError(errors.New(fmt.Sprintf("event '%s' does not exists", eventName)))
	}

	b.triggers[state] = eventName
	return b
}

func (b *Builder) Triggers() i.Triggers {
	return b.triggers
}

func (b *Builder) Callbacks() fsm.Callbacks {
	return b.callbacks
}

func (b *Builder) AfterEventCallback(eventName string, callback fsm.Callback) *Builder {
	return b.addEventCallback("after", eventName, callback)
}

func (b *Builder) BeforeEventCallback(eventName string, callback fsm.Callback) *Builder {
	return b.addEventCallback("before", eventName, callback)
}

func (b *Builder) BeforeAllEventsCallback(callback fsm.Callback) *Builder {
	b.callbacks["before_event"] = callback
	return b
}

func (b *Builder) AfterAllEventsCallback(callback fsm.Callback) *Builder {
	b.callbacks["after_event"] = callback
	return b
}

func (b *Builder) LeaveStateCallback(state string, callback fsm.Callback) *Builder {
	return b.addStateCallback("leave", state, callback)
}

func (b *Builder) EnterStateCallback(state string, callback fsm.Callback) *Builder {
	return b.addStateCallback("enter", state, callback)
}

func (b *Builder) LeaveAllStatesCallback(callback fsm.Callback) *Builder {
	b.callbacks["leave_state"] = callback
	return b
}

func (b *Builder) EnterAllStatesCallback(callback fsm.Callback) *Builder {
	b.callbacks["enter_state"] = callback
	return b
}

func (b *Builder) IsValid() bool {
	return len(b.errors) == 0
}

func getStateFromEvent(event fsm.EventDesc) []string {
	states := append(event.Src, []string{event.Dst}...)
	return states
}

func getStateFromEvents(events fsm.Events) []string {
	var states []string
	for _, event := range events {
		eventStates := getStateFromEvent(event)
		for _, state := range eventStates {
			if utils.IndexOf(state, states) == -1 {
				states = append(states, state)
			}
		}
	}
	return states
}

func (b *Builder) addAllStateToEventSourceState(events []string) {
	for index, event := range b.events {
		if utils.IndexOf(event.Name, events) != -1 {
			b.events[index].Src = b.States()
		}
	}
}

func (b *Builder) addStateFromEvent(event fsm.EventDesc) {
	eventStates := getStateFromEvent(event)
	for _, state := range eventStates {
		b.AddUniqueState(state)
	}
}

func (b *Builder) appendSourceStateToEvent(eventName string, src string) *Builder {
	for index, event := range b.events {
		if event.Name == eventName {
			b.events[index].Src = append(b.events[index].Src, src)
		}
	}
	return b
}

func (b *Builder) addEventCallback(key string, eventName string, callback fsm.Callback) *Builder {
	if !b.HasEventByName(eventName) {
		b.AddError(errors.New(fmt.Sprintf("event '%s' does not exists", eventName)))
	} else {
		b.callbacks[key+"_"+eventName] = callback
	}

	return b
}

func (b *Builder) addStateCallback(key string, state string, callback fsm.Callback) *Builder {
	if !b.HasState(state) {
		b.AddError(errors.New(fmt.Sprintf("state '%s' does not exists", state)))
	} else {
		b.callbacks[key+"_"+state] = callback
	}

	return b
}
