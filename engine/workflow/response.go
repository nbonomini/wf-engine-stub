package workflow

import "ftsystem.com/wf-engine-stub/engine/interfaces"

type Response struct {
	Code         interfaces.Status
	ErrorMessage string
	Result       interface{}
}
