// Package workflow contains all workflow struct and utilities
package workflow

import (
	"context"
	"errors"
	"fmt"
	"ftsystem.com/wf-engine-stub/engine/interfaces"
	"github.com/looplab/fsm"
)

type Recursion int

const (
	Sync int = iota
	Async
)

const (
	Cyclic Recursion = iota
	Finite
)

const (
	MaxErrorRetry = 3
)

type Workflow struct {
	Name           string
	Engine         interfaces.IEngine
	FSM            *fsm.FSM
	CommandChannel chan string
	Type           int
	ctx            context.Context
	cancel         context.CancelFunc
	Triggers       interfaces.Triggers
}

func NewWorkflow(engine interfaces.IEngine, name string, workflowType int) interfaces.IWorkflow {
	return &Workflow{
		Engine:         engine,
		CommandChannel: make(chan string),
		Name:           name,
		Type:           workflowType,
	}
}

func NewAsyncWorkflow(engine interfaces.IEngine, name string) interfaces.IWorkflow {
	return NewWorkflow(engine, name, Async)
}

func NewSyncWorkflow(engine interfaces.IEngine, name string) interfaces.IWorkflow {
	return NewWorkflow(engine, name, Sync)
}

func (w *Workflow) getNextEvent() (string, bool) {

	cs := w.GetState()
	nextEvent, ok := w.Triggers[cs]
	return nextEvent, ok
}

func (w *Workflow) GetName() string {
	return w.Name
}

func (w *Workflow) GetCommandChannel() chan string {
	return w.CommandChannel
}

func (w *Workflow) GetResponseChannel() chan interfaces.IWfResponse {
	return w.Engine.GetResponseChannel()
}

func (w *Workflow) GetFsm() interface{} {
	return w.FSM
}

func (w *Workflow) SetFsm(f interface{}) {
	w.FSM = f.(*fsm.FSM)
}

func (w *Workflow) GetTriggers() interfaces.Triggers {
	return w.Triggers
}

func (w *Workflow) SetTriggers(t interfaces.Triggers) {
	w.Triggers = t
}

func (w *Workflow) IsRunning() bool {
	isRunning := !w.FSM.Is("idle") && !w.FSM.Is("completed")
	return isRunning
}

func (w *Workflow) IsCompleted() bool {
	return w.GetState() == "completed"
}

func (w *Workflow) SendError(err error) {
	responseChannel := w.Engine.GetResponseChannel()
	responseChannel <- Response{
		Code:         interfaces.KO,
		ErrorMessage: err.Error(),
	}
}

func (w *Workflow) SendResponse(response interfaces.ICommandResponse) {
	wr := Response{
		Code:         response.Code(),
		ErrorMessage: response.ErrorMessage(),
		Result:       response.Data(),
	}
	w.Engine.GetResponseChannel() <- wr
}

func (w *Workflow) GetState() string {
	return w.FSM.Current()
}

func (w *Workflow) IsSync() bool {
	return w.Type == Sync
}

func (w *Workflow) Reset() error {
	return w.TriggerEvent("reset")
}

func (w *Workflow) TriggerEvent(event string) error {
	return w.FSM.Event(event)
}

//
func (w *Workflow) Stop() error {

	var err error
	if w.FSM.Can("stop") {
		err = w.TriggerEvent("stop")
	}
	return err
}

func (w *Workflow) asyncRun() {

	w.ctx, w.cancel = context.WithCancel(context.Background())
	fmt.Println("AsyncCyclic:Run > Start MACHINE")

	// CALL PRE
	err := w.TriggerEvent("init")
	if err != nil {
		w.SendError(err)
		return
	}

	// START WORKFLOW : scan available states
	for {

		if !w.IsRunning() {
			break
		}

		evt, found := w.getNextEvent()
		if !found {
			//FATAL ERROR
			w.SendError(errors.New(fmt.Sprintf("Next event of %s not found!", w.GetState())))
			return
		}

		err = w.TriggerEvent(evt)
		if err != nil {
			w.SendError(err)
			break
		}
	}

	// CALL POST
	err = w.Stop()
	if err != nil {
		w.SendError(err)
	}
}

func (w *Workflow) syncRun() {

	w.ctx, w.cancel = context.WithCancel(context.Background())

	if w.GetState() == "idle" {
		// CALL PRE
		err := w.TriggerEvent("init")
		if err != nil {
			w.SendError(err)
			return
		}
	}

	if !w.IsRunning() {
		return
	}

	evt, found := w.getNextEvent()
	if !found {
		//FATAL ERROR
		w.SendError(errors.New(fmt.Sprintf("Next event of %s not found!", w.GetState())))
		return
	}

	err := w.TriggerEvent(evt)
	if err != nil {
		w.SendError(err)
	}
}

func (w *Workflow) Run() {
	if w.Type == Sync {
		w.syncRun()
	} else {
		w.asyncRun()
	}
}
