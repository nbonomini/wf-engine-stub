package workflow

type Event struct {
	From, Name, To string
}

type StartingEvent struct {
	Name, To string
}

type ResumingEvent struct {
	From, Name string
}
