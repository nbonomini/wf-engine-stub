// Package adapter contains shared feature for device adapter microservices
package adapter

import (
	"ftsystem.com/wf-engine-stub/engine/channel"
	"ftsystem.com/wf-engine-stub/engine/engine"
	"ftsystem.com/wf-engine-stub/engine/interfaces"
)

// Adapter creates one or more engine instance that will manage multiple workflows through device defined channels
type Adapter struct {
	engineInstances map[string]*engine.Engine
}

// NewAdapter Creates an Adapter instance
func NewAdapter() *Adapter {
	return &Adapter{}
}

// SetEngineInstance adds a given engine to the adapter engine list mapped by device key
func (a *Adapter) SetEngineInstance(instance *engine.Engine) {
	if a.engineInstances == nil {
		a.engineInstances = map[string]*engine.Engine{}
	}
	a.engineInstances[instance.DeviceKey] = instance
}

// EngineInstance retrieves engine instance from adapter engine list according to the device key
func (a *Adapter) EngineInstance(deviceKey string) *engine.Engine {
	return a.engineInstances[deviceKey]
}

// FetchConfiguratorDeviceInstances calls configuration manager microservice and retrieve device information
func (a *Adapter) FetchConfiguratorDeviceInstances(deviceType string) []map[string]interface{} {
	// TODO call ConfigurationManager to retrieve device instance list
	return []map[string]interface{}{
		0: {
			"address": "127.0.0.1",
			"port":    51000,
			"key":     "myDeviceKey",
			"type":    deviceType,
		},
	}
}

// CreateDeviceChannels builds and return a map of channels according to given parameters.
//
// For each channel type the function will check if parameters are available and execute the dedicated build function (ex. createGrpcChannel)
func (a *Adapter) CreateDeviceChannels(channelParameters *channel.Parameters) (map[string]interfaces.IChannel, error) {

	channels := map[string]interfaces.IChannel{}

	if parameters, ok := channelParameters.Parameters["grpc"]; ok {
		grpcParameters := parameters.(*channel.GrpcParameters)
		channelInstance, err := a.createGrpcChannel(grpcParameters)
		if err != nil {
			return nil, err
		}
		channels["grpc"] = channelInstance
	}
	/*if parameters, ok := channelParameters.Parameters["opcua"]; ok {
		channels["opcua"] = a.createOpcUaChannel(parameters)
	}*/

	return channels, nil
}

// createGrpcChannel builds a GrpcChannel given the address from parameters argument
func (a *Adapter) createGrpcChannel(parameters *channel.GrpcParameters) (*channel.GrpcChannel, error) {
	address := parameters.FullAddress()
	return channel.NewGrpcChannel(address, parameters.ClientInstanceFn)
}

/*func (a *Adapter) createOpcUaChannel(parameters channel.OpcUaParameters) {

}*/
