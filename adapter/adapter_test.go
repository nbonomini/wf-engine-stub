package adapter_test

import (
	"bou.ke/monkey"
	"context"
	"errors"
	"ftsystem.com/wf-engine-stub/adapter"
	"ftsystem.com/wf-engine-stub/engine/channel"
	"ftsystem.com/wf-engine-stub/engine/engine"
	"ftsystem.com/wf-engine-stub/engine/interfaces"
	"ftsystem.com/wf-engine-stub/engine/log"
	"ftsystem.com/wf-engine-stub/engine/workflow"
	"google.golang.org/grpc"
	. "gopkg.in/check.v1"
	"testing"
)

func TestAdapter(t *testing.T) { TestingT(t) }

type AdapterSuite struct{}

var _ = Suite(&AdapterSuite{})

func createFakeEngine() *engine.Engine {
	channels := map[string]interfaces.IChannel{}
	getWorkflowInstanceFn := func(ctx context.Context, name string, engine *engine.Engine) interfaces.IWorkflow {
		return createMyWorkflowInstance(engine, name)
	}
	return engine.NewEngine(channels, "randomDeviceKey", getWorkflowInstanceFn, &log.Logger{})
}

func createMyWorkflowInstance(engine *engine.Engine, name string) interfaces.IWorkflow {
	return workflow.NewAsyncWorkflow(engine, name)
}

func (s *AdapterSuite) TestNewAdapter(c *C) {
	var expectedAdapter *adapter.Adapter
	c.Assert(adapter.NewAdapter(), FitsTypeOf, expectedAdapter)
}

func ExampleAdapter_SetEngineInstance() {
	adapterInstance := adapter.NewAdapter()
	channels := map[string]interfaces.IChannel{}
	getWorkflowInstanceFn := func(ctx context.Context, name string, engine *engine.Engine) interfaces.IWorkflow {
		return createMyWorkflowInstance(engine, name)
	}
	engineInstance := engine.NewEngine(channels, "randomDeviceKey", getWorkflowInstanceFn, &log.Logger{})
	adapterInstance.SetEngineInstance(engineInstance)
}

func (s *AdapterSuite) TestSetEngineInstance(c *C) {
	adapterInstance := adapter.NewAdapter()
	engineInstance := createFakeEngine()
	adapterInstance.SetEngineInstance(engineInstance)
	var expectedEngine *engine.Engine
	c.Assert(adapterInstance.EngineInstance("randomDeviceKey"), FitsTypeOf, expectedEngine)
}

func (s *AdapterSuite) TestFetchConfiguratorDeviceInstances(c *C) {
	// TODO add monkey patch when the method will be correctly implemented
	adapterInstance := adapter.NewAdapter()
	result := adapterInstance.FetchConfiguratorDeviceInstances("inspectionFirmware")
	var expectedResult []map[string]interface{}
	c.Assert(result, FitsTypeOf, expectedResult)
}

func (s *AdapterSuite) TestCreateDeviceChannels(c *C) {
	adapterInstance := adapter.NewAdapter()
	channelParameters := channel.NewChannelParameters()
	deviceChannels, err := adapterInstance.CreateDeviceChannels(channelParameters)
	var expectedDeviceChannels map[string]interfaces.IChannel
	c.Assert(deviceChannels, FitsTypeOf, expectedDeviceChannels)
	c.Assert(err, IsNil)
}

func ExampleAdapter_CreateDeviceChannels() {
	adapterInstance := adapter.NewAdapter()
	channelParameters := channel.NewChannelParameters()
	type fakeConnClient struct{}
	channelParameters.SetGrpc("127.0.0.1", 8080, func(c grpc.ClientConnInterface) interface{} {
		return &fakeConnClient{}
	})
	_, _ = adapterInstance.CreateDeviceChannels(channelParameters)
}

func (s *AdapterSuite) TestCreateGrpcChannel(c *C) {
	defer monkey.UnpatchAll()
	monkey.Patch(grpc.Dial, func(target string, opts ...grpc.DialOption) (*grpc.ClientConn, error) {
		conn := &grpc.ClientConn{}
		return conn, nil
	})
	adapterInstance := adapter.NewAdapter()
	channelParameters := channel.NewChannelParameters()
	type fakeConnClient struct{}
	channelParameters.SetGrpc("127.0.0.1", 8080, func(c grpc.ClientConnInterface) interface{} {
		return &fakeConnClient{}
	})
	deviceChannels, err := adapterInstance.CreateDeviceChannels(channelParameters)
	var expectedGrpcChannel *channel.GrpcChannel
	c.Assert(deviceChannels["grpc"], FitsTypeOf, expectedGrpcChannel)
	c.Assert(err, IsNil)
}

func (s *AdapterSuite) TestCreateGrpcChannelError(c *C) {
	defer monkey.UnpatchAll()
	monkey.Patch(grpc.Dial, func(target string, opts ...grpc.DialOption) (*grpc.ClientConn, error) {
		err := errors.New("connection error")
		return nil, err
	})
	adapterInstance := adapter.NewAdapter()
	channelParameters := channel.NewChannelParameters()
	type fakeConnClient struct{}
	channelParameters.SetGrpc("127.0.0.1", 8080, func(c grpc.ClientConnInterface) interface{} {
		return &fakeConnClient{}
	})
	deviceChannels, err := adapterInstance.CreateDeviceChannels(channelParameters)
	c.Assert(deviceChannels, IsNil)
	c.Assert(err, NotNil)
}
