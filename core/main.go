package main

import (
	"context"
	"fmt"
	pb "ftsystem.com/wf-engine-stub/proto"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
	"log"
	"net"
)

const (
	local_port     = ":50052"
	engine_address = "localhost:50051"
)

type server struct{}

func (s server) ProcessReply(ctx context.Context, request *pb.Request) (*pb.Empty, error) {
	fmt.Printf("Core process reply result: %s\n", request.Param)
	return &pb.Empty{}, nil
}

var client pb.WorkflowManagerClient

func (s server) Start(ctx context.Context, request *pb.Request) (*pb.Reply, error) {
	fmt.Printf("CORE : Start ...\n")
	r, err := client.Start(context.Background(), &pb.Request{Param: request.Param})
	fmt.Printf("CORE : Start result\n")
	if err == nil {
		fmt.Printf(r.Param)
	} else {
		fmt.Printf(err.Error())
	}

	return &pb.Reply{Param: "Started"}, nil
}

func (s server) Stop(ctx context.Context, request *pb.Request) (*pb.Reply, error) {
	r, err := client.Stop(context.Background(), &pb.Request{Param: request.Param})
	fmt.Printf("\nCORE : Stop result")
	if err == nil {
		fmt.Printf(r.Param)
	} else {
		fmt.Printf(err.Error())
	}
	return &pb.Reply{Param: "Stop"}, nil
}

func (s server) ExecuteCommand(ctx context.Context, request *pb.Request) (*pb.Reply, error) {
	panic("implement me")
}

func main() {
	// Set up a connection to the server.

	go startServer()

	fmt.Println("Start client")

	startClient()

	var s string
	fmt.Scanln(&s)

	/*
		lis, err := net.Listen("tcp", local_port)
		if err != nil {
			log.Fatalf("failed to listen: %v", err)
		}
		s := grpc.NewServer()
		pb.RegisterWorkflowManagerServer(s, &server{})

		// Register reflection service on gRPC server.
		reflection.Register(s)
		if err := s.Serve(lis); err != nil {
			log.Fatalf("failed to serve: %v", err)
		}
		fmt.Printf("SYS UP")
	*/

	/*
		conn, err := grpc.Dial(core_address, grpc.WithInsecure())
		if err != nil {
			log.Fatalf("did not connect: %v", err)
		}
		defer conn.Close()
		c := pb.NewWorkflowManagerClient(conn)

		fmt.Printf("Press to START")

		var s string
		fmt.Scanln(&s)

		c.Start(context.Background() , &pb.Request{Param: "ReadResults"})
	*/
}

func startClient() {
	// Set up a connection to the server.
	conn, err := grpc.Dial(engine_address, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	//defer conn.Close()

	client = pb.NewWorkflowManagerClient(conn)
}

func startServer() {
	lis, err := net.Listen("tcp", local_port)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	s := grpc.NewServer()

	pb.RegisterWorkflowManagerServer(s, &server{})

	// Register reflection service on gRPC server.
	reflection.Register(s)
	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
