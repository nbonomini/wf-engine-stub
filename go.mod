module ftsystem.com/wf-engine-stub

go 1.16

require (
	bou.ke/monkey v1.0.2
	github.com/looplab/fsm v0.2.0
	golang.org/x/net v0.0.0-20210525063256-abc453219eb5
	golang.org/x/tools v0.1.3 // indirect
	google.golang.org/grpc v1.38.0
	google.golang.org/protobuf v1.26.0
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c
)
